<!DOCTYPE html>
<html>
<head>
	<title>Covid19 Tracker</title>

<link rel = "icon" href =  
"https://cdn.pixabay.com/photo/2020/03/27/16/51/virus-4974270_960_720.png" 
        type = "image/x-icon"> 

	<link rel="stylesheet" type="text/css" href="css/style.css">
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
	<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
<script
  src="https://code.jquery.com/jquery-3.5.0.min.js"
  integrity="sha256-xNzN2a4ltkB44Mc/Jz3pT4iU1cmeR0FkXs4pru/JxaQ="
  crossorigin="anonymous"></script>

  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

</head>


<body onload="fetch()">

<!-- Navbar section -->

<div class="main-div">
	<div class="navs">
<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
  <a class="navbar-brand" href="#"><img src="https://cdn.pixabay.com/photo/2020/03/27/16/51/virus-4974270_960_720.png" style="height: 40px; width: 40px;"><span class="ml-2">Covid19 Tracker</span></a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav ml-auto">
      <li class="nav-item active">
        <a class="nav-link" href="#symptom">SYMPTOMS <span class="sr-only">(current)</span></a>
      </li>
      
       <li class="nav-item active">
        <a class="nav-link" href="#precaution">PRECAUTIONS <span class="sr-only">(current)</span></a>
      </li>
          <li class="nav-item active">
        <a class="nav-link" href="#about">ABOUT <span class="sr-only">(current)</span></a>
      </li>
          <li class="nav-item active">
        <a class="nav-link" href="#contact">CONTACT <span class="sr-only">(current)</span></a>
      </li>

    </ul>
  </div>
</nav>
	</div>

<!-- navbar section finished -->
<div class="row container-fluid my-4 " id="precaution">
	<div class="col-12 text-center">
		<h3 style="color: #0275d8;font-weight: 700;">COVID19 LIVE TRACKER OVER THE WORLD</h3>
	</div>
</div>
<!-- header section -->

<div class="row container-fluid">
	<div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-xm-12 text-center">
		<img src="images/main.gif" class="img-fluid mt-4" alt="Responsive image" style="height: 400px;width: 400px;">
	</div>
	<div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-xm-12 ">
		<div class="row mt-5">
			<div class="col-12 text-center">
		<h3>
			<span style="color: #09641C ;font-weight: 700;">Stay Home</span>
			<span style="color: #062D78;font-weight: 700;">Save Lives</span>
		</h3>
		</div>
		</div>

		
		<div class="row">
			<div class="col-12 text-center mt-4">
				<ol class="list-group" style="font-weight: 700;font-size: 18px;">
  <li class="list-group-item">Quick Tips (To prevent CORONA)</li>

  
  <li class="list-group-item list-group-item-primary">Cover your cough</li>
  <li class="list-group-item list-group-item-secondary">KEEP a safe distance and WASH your hand</li>
  <li class="list-group-item list-group-item-success">Drink and Hot Items</li>
  <li class="list-group-item list-group-item-danger">SICK?Call the helpline</li>

</ol>
			</div>
		</div>
	</div>
</div>

<!-- header section finished -->


<!-- all data section -->

<div class="row my-5 ">
	<div class="col-2"></div>
	<div class="col-8">
		<div class="table-responsive">
			<table id="tabval1" class="table text-center table-hover table-striped">
				<tr class="thead-dark">
				<th>TotalConfirmed (World-Wide)</th>
				<th>TotalRecovered</th>
				<th>TotalDeaths</th>
				</tr>
			</table>
		</div>
	</div>
	<div class="col-2"></div>
</div>

<!-- all data section closed -->


<!-- data section -->

<div class="row my-4 ">
	<div class="col-12">
		<div class="table-responsive">
			<table id="tabval" class="table text-center table-hover table-striped">
				<tr class="thead-dark">
				<th>Country</th>
				<th>TotalConfirmed</th>
				<th>TotalRecovered</th>
				<th>TotalDeaths</th>
				<th>NewConfirmed</th>
				<th>NewRecovered</th>
				<th>NewDeaths</th>
				</tr>
			</table>
		</div>
	</div>
</div>

<!-- data section closed -->

<!-- about section -->

<div class="row container-fluid mb-3 text-center" id="about">
	<div class="col-12">
	  	<h3 style="color: #0275d8;font-weight: 700;">
	  		<span>What is </span>
	  		<span style="color: #d9534f;">CORONA ?</span>
	  		<span>Why it is too dangerous ? </span>
	  	</h3>
	</div>
</div>

<div class="row my-5">
	<div class="col-xl-6 col-lg-6 col-sm-12 col-sm-12 col-md-12 text-center">
		<img src="images/corona.png" class="img-fluid" alt="Responsive image" style="height: 400px;width: 700px;"> 
	</div>
	<div class="col-xl-6 col-lg-6 col-sm-12 col-sm-12 col-md-12">
		<h5 class="mt-5">
			Scientists are racing to understand the coronavirus disease, Covid-19, which is now a fast-growing global pandemic
		</h5><br>
		<p class="px-2">
	

	The virus has caused severe respiratory disease in about 20 per cent of patients and killed more than 3 per cent of confirmed cases. Sars killed 10 per cent of infected individuals. Older people, whose immune defences have declined with age, as well as those with underlying health conditions, are much more vulnerable than the young.

		</p px-2><br>
		<p>

	The virus multiplies within the lower respiratory tract, where symptoms develop. Early ones are a fever and cough. Most people will recover within a few days. But about 20 per cent go on to develop serious pneumonia as their lungs become inflamed; they may need a respirator to help them breathe.

		</p>
	</div>
</div>


<!-- about section closed -->

<!-- symptomps section -->


<div class="row container-fluid my-5 text-center" id="symptom">
	<div class="col-12">
	  	<h3 style="color: #0275d8;font-weight: 700;">
	  		<span>Symptoms of</span>
	  		<span style="color: #d9534f;">CORONA </span>
	  		<span>Virus</span>
	  	</h3>
	</div>
</div>
<div class="row container-fluid my-3 text-center">
	<div class="col-12 text-center">
	  	<h5 style="color: #f0ad4e;">
	  		COVID-19 affects different people in different ways. Most infected people will develop mild to moderate symptoms.
	  	</h5>
	</div>
</div>

<div class="row my-5">
	
	<div class="col-xl-6 col-lg-6 col-sm-12 col-sm-12 col-md-12 text-center">
		
	<ul class="list-group px-2" style="font-size: 18px;font-weight: 700;">
  <li class="list-group-item">Problem in Respiration</li>
  <li class="list-group-item list-group-item-primary">Fever</li>
  <li class="list-group-item list-group-item-secondary">Tiredness</li>
  <li class="list-group-item list-group-item-success">Dry Cough</li>
  <li class="list-group-item list-group-item-danger">Aches and Pain</li>
  <li class="list-group-item list-group-item-warning">Nasal Congestion and Runny Nose</li>
  <li class="list-group-item list-group-item-info">Sore Throat and Diarrhoea</li>
 
</ul>



	</div>
	<div class="col-xl-6 col-lg-6 col-sm-12 col-sm-12 col-md-12 text-center">
		<img src="images/covid1.jpg" class="img-fluid" alt="Responsive image" style="height: 400px;width: 700px;"> 
	</div>
</div>


<!-- symptoms section closed -->

<!-- contact us section -->

<div class="row my-4" id="contact">
	<div class="col-12 text-center" style="font-size: 32px;">
		<a href="https://www.facebook.com/profile.php?id=100008157437260" style="color: #000;"><i class="fa fa-facebook-official mx-3" aria-hidden="true"></i></a>
		<a href="https://twitter.com/kishanmaharan11?s=08" style="color: #000;"><i class="fa fa-twitter mx-3" aria-hidden="true"></i></a>
		<a href="https://github.com/kumarkishan070997" style="color: #000;"><i class="fa fa-github mx-3" aria-hidden="true"></i></a>
	</div>
</div>

<!-- contact us section closed -->

<!-- footer section -->
<div class="row" style="background-color: #292b2c">
	<div class="col-12 text-center">
		<p style="color: #fff;" class="pt-2">Made with <i class="fa fa-heart" aria-hidden="true" style="color: red;"></i> , Made for People</p>
	</div>
</div>
<!-- footer section closed -->

</div>


<script>
	function fetch()
	{
		
		$.get("https://api.covid19api.com/summary",

			function (data)
			{
				// console.log(data['Countries'].length);
				var tabval=document.getElementById('tabval');
				var tabval1=document.getElementById('tabval1');
				var y=tabval1.insertRow();
					y.insertCell(0);

				tabval1.rows[1].cells[0].innerHTML=data['Global']['TotalConfirmed'];
					tabval1.rows[1].cells[0].style.background='#0275d8';
					tabval1.rows[1].cells[0].style.color='#fff';
					tabval1.rows[1].cells[0].style.fontWeight='700';
				

					y.insertCell(1);
					tabval1.rows[1].cells[1].innerHTML=data['Global']['TotalRecovered'];
					tabval1.rows[1].cells[1].style.background='#f0ad4e';
					tabval1.rows[1].cells[1].style.fontWeight='700';
			


					y.insertCell(2);
					tabval1.rows[1].cells[2].innerHTML=data['Global']['TotalDeaths'];
					tabval1.rows[1].cells[2].style.background='#5cb85c';
				tabval1.rows[1].cells[2].style.fontWeight='700';


				for(var i=1;i<(data['Countries'].length);i++)
				{
					var x=tabval.insertRow();
					x.insertCell(0);

					tabval.rows[i].cells[0].innerHTML=data['Countries'][i-1]['Country'];
					tabval.rows[i].cells[0].style.background='#0275d8';
					tabval.rows[i].cells[0].style.color='#fff';
					tabval.rows[i].cells[0].style.fontWeight='700';


					x.insertCell(1);
					tabval.rows[i].cells[1].innerHTML=data['Countries'][i-1]['TotalConfirmed'];
					tabval.rows[i].cells[1].style.background='#f0ad4e';
					tabval.rows[i].cells[1].style.fontWeight='700';


					x.insertCell(2);
					tabval.rows[i].cells[2].innerHTML=data['Countries'][i-1]['TotalRecovered'];
					tabval.rows[i].cells[2].style.background='#5cb85c';
tabval.rows[i].cells[2].style.fontWeight='700';

					x.insertCell(3);
					tabval.rows[i].cells[3].innerHTML=data['Countries'][i-1]['TotalDeaths'];
					tabval.rows[i].cells[3].style.background='#d9534f';
tabval.rows[i].cells[3].style.fontWeight='700';

					x.insertCell(4);
					tabval.rows[i].cells[4].innerHTML=data['Countries'][i-1]['NewConfirmed'];
					tabval.rows[i].cells[4].style.background='#f0ad4e';
tabval.rows[i].cells[4].style.fontWeight='700';

					x.insertCell(5);
					tabval.rows[i].cells[5].innerHTML=data['Countries'][i-1]['NewRecovered'];
					tabval.rows[i].cells[5].style.background='#5cb85c';
tabval.rows[i].cells[5].style.fontWeight='700';

					x.insertCell(6);
					tabval.rows[i].cells[6].innerHTML=data['Countries'][i-1]['NewDeaths'];
					tabval.rows[i].cells[6].style.background='#d9534f';
					tabval.rows[i].cells[6].style.fontWeight='700';
				}
			}
			);
	}
</script>	
</body>
</html>